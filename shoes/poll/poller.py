import django
import os
import sys
import time
import json
import requests

sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "shoes_project.settings")
django.setup()

# Import models from shoes_rest, here.
# from shoes_rest.models import Something
from shoes_rest.models import BinVO

def get_bin_info():
    response = requests.get('http://wardrobe-api:8000/api/bins/')
    content = json.loads(response.content)
    for bin in content["bins"]:
        print(bin["href"])
        BinVO.objects.update_or_create(
            import_href=bin["href"]
        )

def poll():
    while True:
        print('Shoes poller polling for data')
        try:
            # Write your polling logic, here
            get_bin_info()
            pass
        except Exception as e:
            print(e, file=sys.stderr)
        time.sleep(5)


if __name__ == "__main__":
    poll()
