from django.shortcuts import render
from django.views.decorators.http import require_http_methods
from .models import BinVO, Shoe
from common.json import ModelEncoder
from django.http import JsonResponse
import json


# Create your views here.

class BinVOEncoder(ModelEncoder):
    model = BinVO
    properties = [
        "import_href"
    ]

class ShoeListEncoder(ModelEncoder):
    model = Shoe
    properties = [
        "id",
        "manufacturer",
        "model_name",
        "color",
        "picture_url",
        "bin"
    ]
    encoders = {
        "bin": BinVOEncoder()
    }

@require_http_methods(["GET", "POST"])
def list_shoes(request):
    try:
        if request.method == "GET":
            shoes = Shoe.objects.all()
            return JsonResponse(
                {"shoes": shoes},
                encoder=ShoeListEncoder
            )
        else:
            content = json.loads(request.body)
            try:
                bin_href = content["bin"]
                bin = BinVO.objects.get(import_href=bin_href)
                content["bin"] = bin
            except BinVO.DoesNotExist:
                return JsonResponse(
                    {"message": "Invalid bin_number id"},
                    status=400
                )
            shoe = Shoe.objects.create(**content)
            return JsonResponse(
                shoe,
                encoder=ShoeListEncoder,
                safe=False
            )
    except json.JSONDecodeError:
        return JsonResponse(
            {"message": "Json incorrectly formatted in body"},
            status=400
        )


@require_http_methods(["DELETE"])
def delete_shoe(request, pk):
    try:
        if request.method == "DELETE":
            count, _ = Shoe.objects.filter(id=pk).delete()
            return JsonResponse({"deleted": count > 0})
    except json.JSONDecodeError:
        return JsonResponse(
            {"message": "improper deletion, likely PK or does not exist"}
        )
