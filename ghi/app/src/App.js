import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import HatList from './HatList';
import ShoeList from './ShoeList'
import ShoeCreateForm from './ShoeCreateForm'
import HatForm from './HatForm';
import BinsList from './BinsList'
import ShoeDetail from './ShoeDetail';

function App() {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="/hats" element={<HatList />} />
          <Route path="/shoes" element={<ShoeList />} />
          <Route path="shoes/create" element={<ShoeCreateForm />} />
          <Route path="/hats/new" element={<HatForm />} />
          <Route path="/bins" element={<BinsList />} />
          <Route path="/shoes/detail" element={<ShoeDetail />} />
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
