import React, { useEffect, useState } from 'react'
import { Link } from 'react-router-dom'

function BinsList() {

    const [bins, setBins] = useState([])

    const fetchData = async () => {
        const url = "http://localhost:8100/api/bins/"
        const response = await fetch(url);
        try{
            if (response.ok) {
            const data = await response.json()
            setBins(data.bins)
            }
        } catch (error) {
            console.error("Error in retrieving bins: fetchData", error)
        }
        }

    useEffect(() => {
        fetchData();
    }, []);

    async function deleteBin(binId) {
        const url = `http://localhost:8100/api/bins/${binId}/`
        const fetchConfig = {method: "DELETE"}
        const response = await fetch(url, fetchConfig)
        try{
            if (response.ok) {
                fetchData()
            }
        } catch (error) {
            console.error("Error in deleting bin: deleteBin", error)
        }
    }


    return (
        <div>
            <div><Link to="/shoes">Return to Shoe List</Link></div>
            <div><Link to="/shoes/create">Create a new Shoe</Link></div>
                <table className="table table-striped m-3">
                <thead>
                    <tr>
                    <th>Sub-Bin</th>
                    <th>Bin</th>
                    <th>Closet</th>
                    <th>Sub-Bin Size</th>
                    <th>Delete Sub-Bin</th>
                    </tr>
                </thead>
                <tbody>
                    {bins.map(bin => {
                    return (
                        <tr key={ bin.href }>
                            <td>{ `${bin.href.match(/\d+/)}` }</td>
                            <td>{ bin.bin_number }</td>
                            <td>{ bin.closet_name }</td>
                            <td>{ bin.bin_size }</td>
                            <td><button className="btn btn-outline-danger end" onClick={ () => deleteBin(bin.id)} >Delete</button></td>
                        </tr>
                    );
                    })}
                </tbody>
                </table>
                <div><Link to="/shoes">Return to Shoe List</Link></div>
                <div><Link to="/shoes/create">Create a new Shoe</Link></div>
        </div>
    )
}
export default BinsList
