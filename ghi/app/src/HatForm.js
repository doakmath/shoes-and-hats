import React, { useEffect, useState } from 'react';

function HatForm() {
    const [locations, setLocations] = useState([])

    const [formData, setFormData] = useState({
        fabric: '',
        style: '',
        color: '',
        picture_url: '',
        location: '',
    })

    const handleFormChange = (event) => {
        const inputName = event.target.name
        const value = event.target.value
        setFormData({
            ...formData,
            [inputName]: value
        })
    }

    const fetchData = async () => {
        const url = 'http://localhost:8100/api/locations/'
        try {
            const response = await fetch(url)
            if (response.ok) {
                const data = await response.json()
                setLocations(data.locations)
            } else {
                console.error('Failed to fetch locations:', response.status, response.statusText)
            }
        } catch (error) {
            console.error('Error fetching locations', error)
        }
    }

    useEffect(() => {
        fetchData()
    }, [])

    const handleSubmit = async (event) => {
        event.preventDefault();
        const hatUrl = 'http://localhost:8090/api/hats/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(formData),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        try {
            const response = await fetch(hatUrl, fetchConfig);
            if (response.ok) {
                const newHat = await response.json();

                setFormData({
                    fabric: '',
                    style: '',
                    color: '',
                    picture_url: '',
                    location: '',
                })
            } else {
                console.error('Server responded with an error:', response.status, response.statusText);
            }
        } catch (error) {
            console.error('Error parsing JSON response:', error);
        }
    }

    return (
        <div className="my-5 container">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Create a new hat</h1>
                    <form onSubmit={handleSubmit} id="create-hat-form">
                        <div className="form-floating mb-3">
                            <input value={formData.fabric} onChange={handleFormChange} placeholder="fabric" required type="text" id="fabric" className="form-control"
                                name="fabric" />
                            <label htmlFor="fabric">Fabric</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input value={formData.style} onChange={handleFormChange} placeholder="Style" required type="text" id="style"
                                className="form-control" name="style" />
                            <label htmlFor="style">Style</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input value={formData.color} onChange={handleFormChange} placeholder="Color" required type="text" id="color"
                                className="form-control" name="color" />
                            <label htmlFor="color">Color</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input value={formData.picture_url} onChange={handleFormChange} placeholder="Picture Url" type="text" id="picture_url"
                                className="form-control" name="picture_url" />
                            <label htmlFor="picture_url">Picture Url (Optional)</label>
                        </div>
                        <div className="mb-3">
                            <select value={formData.location} onChange={handleFormChange} required id="location" className="form-select" name="location">
                                <option value="">Choose a location</option>
                                {locations.map(location => {
                                    return (
                                        <option key={location.href} value={location.href}>
                                            {`${location.closet_name} - ${location.section_number}/${location.shelf_number}`}
                                        </option>
                                    );
                                })}
                            </select>
                        </div>
                        <button className="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
        </div>
    )
}

export default HatForm;
