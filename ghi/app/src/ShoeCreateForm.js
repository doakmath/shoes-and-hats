import React, { useEffect, useState } from 'react'
import { Link } from 'react-router-dom'

function ShoeCreateForm() {

    const [bins, setBins] = useState([])
    const [bin, setBin] = useState()
    const [manufacturer, setManufacturer] = useState("")
    const [modelName, setModelName] = useState("")
    const [color, setColor] = useState("")
    const [picture, setPicture] = useState("")


    const handleBinChange = (event) => {
        const value = event.target.value
        setBin(value)
    }

    const handleManufacturerChange = (event) => {
        const value = event.target.value
        setManufacturer(value)
    }

    const handleModelNameChange = (event) => {
        const value = event.target.value
        setModelName(value)
    }

    const handleColorChange = (event) => {
        const value = event.target.value
        setColor(value)
    }

    const handlePictureChange = (event) => {
        const value = event.target.value
        setPicture(value)
    }

    const fetchData = async () => {
        const url = "http://localhost:8100/api/bins/"
        const response = await fetch(url);
        try {
            if (response.ok) {
            const data = await response.json()
            setBins(data.bins)
            }
        } catch (error) {
            console.error("Error retrieving bins: fetchData", error)
        }
        }

    useEffect(() => {
        fetchData()
    }, [])

    const handleSubmit = async (event) => {
        event.preventDefault()
        const data = {}
        data.manufacturer = manufacturer
        data.model_name = modelName
        data.color = color
        data.picture_url = picture
        data.bin = bin
        const conferenceUrl = 'http://localhost:8080/api/shoes/'
        const fetchConfig = {
            method: "POST",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        }
        const response = await fetch(conferenceUrl, fetchConfig)
        try{
            if (response.ok) {
                const newShoe = await response.json()
                setBin('')
                setManufacturer('')
                setModelName('')
                setColor('')
                setPicture("")
                created()
            }
        } catch (error) {
            console.error("Error in submission event: handleSubmit")
        }
    }

    function created() {
        return (
            alert(`You added a pair of Shoes! \nYou can see it in 'Shoes'\nOr click OK and add another!`)
        )
    }


    return (
        <div className="row">
            <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
                <h1>Create a Shoe</h1>
                <form onSubmit={handleSubmit} id="create-conference-form">
                 <div className="mb-3">
                    <select value={bin} onChange={handleBinChange} required id="bin" name="bin" className="form-select">
                    <option value="">Choose a: [Sub Bin]----[Bin]----[Closet] combination</option>
                    {bins.map(bin => {
                        return (
                        <option key={bin.href} value={bin.href}>Sub Bin {bin.href.match(/\d+/)}----Bin: {bin.bin_number}----Closet {bin.closet_name}</option>
                        )})}
                    </select>
                </div>
                <div className="form-floating mb-3">
                    <input value={manufacturer} onChange={handleManufacturerChange} required type="text" name="manufacturer" id="manufacturer" className="form-control" />
                    <label htmlFor="manufacturer">Manufacturer</label>
                </div>
                <div className="form-floating mb-3">
                    <input value={modelName} onChange={handleModelNameChange} required type="text" name="modelName" id="modelName" className="form-control" />
                    <label htmlFor="modelName">Model</label>
                </div>
                <div className="form-floating mb-3">
                    <input value={color} onChange={handleColorChange} placeholder="color" required type="text" name="color" id="color" className="form-control" />
                    <label htmlFor="color">Color</label>
                </div>
                <div className="form-floating mb-3">
                    <input value={picture} onChange={handlePictureChange}placeholder="picture" type="text" name="picture" id="picture" className="form-control" />
                    <label htmlFor="picture">Picture URL (Optional)</label>
                </div>
                <button className="btn btn-primary">Create</button>
                <div>
                    <Link to="/shoes">Click to see Shoe List</Link>
                </div>
                </form>
            </div>
            </div>
        </div>
    )
}


export default ShoeCreateForm
