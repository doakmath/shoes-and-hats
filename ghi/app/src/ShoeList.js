import { useState, useEffect } from 'react'
import { Link, useLocation } from 'react-router-dom';


function ShoeList() {

    const [shoes, setShoes] = useState([])
    const [bins, setBins] = useState([])
    const [subBins, setSubBins] = useState('')

    const getData = async () => {
        const response = await fetch('http://localhost:8080/api/shoes/')
        try {
            if (response.ok) {
                const { shoes } = await response.json()
                setShoes(shoes)
            } else {
                console.error("problem in ShoeList getData")
        }
        } catch (error) {
            console.error("Error retrieving shoes: getData", error)
        }
    }

    const fetchData = async () => {
        const url = "http://localhost:8100/api/bins/"
        const response = await fetch(url);
        try {
            if (response.ok) {
            const data = await response.json()
            setBins(data.bins)
            }
        } catch (error) {
            console.error("Error retrieving bins: fetchData", error)
        }
        }

    useEffect(() => {
        getData()
        fetchData();
    }, []);

    async function deleteShoe(shoeId) {
        const url = `http://localhost:8080/api/shoes/${shoeId}/`
        const fetchConfig = {method: "DELETE"}
        const response = await fetch(url, fetchConfig)
        try {
            if (response.ok) {
                getData()
            }
        } catch (error) {
            console.error("Error deleting shoe: deleteShoe", error)
        }
    }

    async function handleSubBins(subBinId) {
        const url = `http://localhost:8100${subBinId}`
        const response = await fetch(url)
        const data = await response.json()
        try {
            if (response.ok) {
                setSubBins(data)
            }
        } catch (error) {
            console.error("Error retrieving specific Sub-Bin: handleSubBins", error)
        }
    }

    const handleSubBinsChange = (event) => {
        const value = event.target.value
        handleSubBins(value)
    }


    return(
        <div className="my-5 container">
            <div className="row">
                <h1>ShoeList</h1>
                <table className="table table-striped m-3">
                <thead>
                    <tr>
                    <th>Sub-Bin</th>
                    <th>Shoe Id #</th>
                    <th>Manufacturer</th>
                    <th>Model</th>
                    <th>Color</th>
                    <th>View Shoe Detail</th>
                    <th>Picture</th>
                    <th>Delete</th>
                    </tr>
                </thead>
                <tbody>
                    {shoes.map(shoe => {
                    return (
                        <tr key={ shoe.id }>
                            <td>{ `Sub-Bin ${shoe.bin.import_href.match(/\d+/)}` }</td>
                            <td>{ shoe.id }</td>
                            <td>{ shoe.manufacturer }</td>
                            <td>{ shoe.model_name }</td>
                            <td>{ shoe.color }</td>
                            <td><Link to="/shoes/detail">View</Link></td>
                            <td><img height='75' width='75' src={shoe.picture_url} alt="a picture of a shoe goes here" /></td>
                            <td><button className="btn btn-outline-danger end" onClick={ () => deleteShoe(shoe.id)} >Delete</button></td>
                        </tr>
                    );
                    })}
                </tbody>
                </table>
                <div>
                    <h4>Inventory View Options</h4>
                </div>
                <ul>
                    <li>
                        <div><Link to="/bins"><button>Click to View: Sub-Bin/Bin/Closet Info</button></Link></div>
                    </li>
                    <li>
                        <div className="mb-3">
                            <select value={subBins}  onChange={handleSubBinsChange} required id="subBins" name="subBins" className="form-select">
                            <option value="">Choose a Sub-Bin to see Sub-Bin/Bin/Closet Info</option>
                            {bins.map(subBin => {
                                return (
                                <option key={subBin.href} value={subBin.href}>Sub-Bin {subBin.href.match(/\d+/)}</option>
                                )})}
                            </select>
                        </div>
                    </li>
                    <div>
                        Sub-Bin: {subBins.id}----Bin: {subBins.bin_number}----Closet: {subBins.closet_name}----Bin Size: {subBins.bin_size}
                    </div>
                </ul>
            </div>
        </div>
    )

}


export default ShoeList
