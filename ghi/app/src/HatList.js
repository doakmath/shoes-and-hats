import React, { useEffect, useState } from 'react';

function HatList() {
    const [hats, setHats] = useState([])
    const [locations, setLocations] = useState([])

    const fetchHats = async () => {
        const url = 'http://localhost:8090/api/hats/'
        const response = await fetch(url)
        try {
            if (response.ok) {
                const data = await response.json()
                setHats(data.hats)
            } else {
                console.error('Failed to fetch hats:', response.status, response.statusText)
            }
        } catch (error) {
            console.error('Error fetching hats', error)
        }
    }

    const fetchLocations = async () => {
        const url = 'http://localhost:8100/api/locations/'
        try {
            const response = await fetch(url)
            if (response.ok) {
                const data = await response.json()
                setLocations(data.locations)
            } else {
                console.error('Failed to fetch locations:', response.status, response.statusText)
            }
        } catch (error) {
            console.error('Error fetching locations', error)
        }
    }

    useEffect(() => {
        fetchHats()
        fetchLocations()
    }, [])

    const handleDelete = async (id) => {
        const url = `http://localhost:8090/api/hats/${id}`;
        const fetchConfig = {
            method: "delete",
        };
        try {
            const response = await fetch(url, fetchConfig);
            if (response.ok) {
                fetchHats()
            } else {
                console.error('Failed to delete hat:', response.status, response.statusText)
            }
        } catch (error) {
            console.error('Error deleting hat', error)
        }
    }

    const getLocationByHref = (import_href) => {
        return locations.find((location) => location.href === import_href);
    };

    return (
        <table className="table table-striped">
            <thead>
                <tr>
                    <th>Image</th>
                    <th>Style</th>
                    <th>Fabric</th>
                    <th>Color</th>
                    <th>Location</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                {hats.map(hat => {
                    const hat_details = `${hat.color} ${hat.fabric} ${hat.style} hat`
                    const location = getLocationByHref(hat.location.import_href)
                    const location_details = location ? `${location.closet_name} - ${location.section_number}/${location.shelf_number}` : `Location at ${hat.location.import_href} not found.`;
                    return (
                        <tr key={hat.id}>
                            <td>{hat.picture_url ? (<img src={hat.picture_url} alt={hat_details} style={{ maxHeight: '100px' }} />) : null}</td>
                            <td>{hat.style}</td>
                            <td>{hat.fabric}</td>
                            <td>{hat.color}</td>
                            <td>{location_details}</td>
                            <td><button onClick={() => handleDelete(hat.id)} className="btn btn-danger">Remove</button></td>
                        </tr>
                    )
                })}
            </tbody>
        </table>
    )
}

export default HatList
