from django.urls import path

from .api_views import api_list_hats, api_delete_hat

urlpatterns = [
    path("hats/", api_list_hats, name="api_list_hats"),
    path("hats/<int:pk>/", api_delete_hat, name="api_delete_hat"),
]
