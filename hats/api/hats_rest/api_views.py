from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json

from common.json import ModelEncoder
from .models import LocationVO, Hat


class LocationVODetailEncoder(ModelEncoder):
    model = LocationVO
    properties = [
        "import_href",
    ]

class HatEncoder(ModelEncoder):
    model = Hat
    properties = [
        "id",
        "fabric",
        "style",
        "color",
        "location",
        "picture_url",
    ]

    encoders = {
        "location": LocationVODetailEncoder(),
    }


@require_http_methods(["GET", "POST"])
def api_list_hats(request):
    try:
        if request.method == "GET":
            hats = Hat.objects.all()
            return JsonResponse(
                {"hats": hats},
                encoder=HatEncoder
            )

        else:
            content = json.loads(request.body)

            try:
                location_href = content["location"]
                location = LocationVO.objects.get(import_href=location_href)
                content["location"] = location
            except LocationVO.DoesNotExist:
                return JsonResponse(
                    {"message": "Invalid location id"},
                    status=400
                )

            hat = Hat.objects.create(**content)
            return JsonResponse(
                hat,
                encoder=HatEncoder,
                safe=False,
            )
    except json.JSONDecodeError:
        return JsonResponse(
            {"message": "Invalid JSON in request body"},
            status=400
        )

@require_http_methods(["DELETE"])
def api_delete_hat(request, pk):
    count, _ = Hat.objects.filter(id=pk).delete()
    return JsonResponse({"deleted": count > 0})
